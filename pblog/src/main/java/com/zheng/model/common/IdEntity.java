package com.zheng.model.common;

import lombok.Data;

/**
 * Created by user on 2016/4/5.
 */
@Data
public abstract class IdEntity {
    protected Long id;

}
