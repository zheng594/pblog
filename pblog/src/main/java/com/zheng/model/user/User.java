package com.zheng.model.user;

import com.zheng.model.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by user on 2016/3/31.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class User extends BaseEntity implements Serializable{
    private String userName;
    private String password;
    private String mail;
    private Integer imageId;
    private String phone;
    private Byte sex;
    private Date birthday;

}
