package com.zheng.dao.common;

import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by user on 2016/4/5.
 */
public interface BaseDao<T> {

    /**
     * 查询总数
     *
     * @param param
     * @return
     */
    Integer selectCount(Map<String, Object> param);

    /**
     * 查询
     *
     * @param parameters
     * @return
     */
    List<T> select(Map<String, Object> parameters);

    List<T> select(Object obj);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    T selectById(Object id);

    /**
     * 根据IDS查询
     *
     * @param ids
     * @return
     */
    List<T> selectByIds(@Param("ids") Collection<Long> ids);

    /**
     * 根据ID删除
     *
     * @param id
     * @return
     */
    int deleteById(Object id);

    /**
     * 根据IDS批量删除
     *
     * @param list
     * @return
     */
    int deleteByIds(Object[] list);

    /**
     * 删除
     *
     * @param parameters
     * @return
     */
    int delete(Map<String, Object> parameters);

    /**
     * 添加
     *
     * @param t
     * @return
     */
    int insert(T t);

    /**
     * 通过ID更新
     *
     * @param t
     * @return
     */
    int updateById(T t);

}
