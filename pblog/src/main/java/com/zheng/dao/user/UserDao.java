package com.zheng.dao.user;

import com.zheng.dao.common.BaseDao;
import com.zheng.dao.common.MyBatisRepository;
import com.zheng.model.user.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by user on 2016/4/5.
 */
@Repository
public interface UserDao extends BaseDao<User>{
    //public User getUser(@Param("user") User user);

    //public List<User> selectByIds(@Param("idList")List<Integer> idList);

    public Integer getUserCount();


}
