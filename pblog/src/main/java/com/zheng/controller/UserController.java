package com.zheng.controller;

import com.zheng.model.user.User;
import com.zheng.service.impl.UserService;
import com.zheng.common.JedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by user on 2016/4/1.
 */
@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private JedisUtil jedisUtil;

    @RequestMapping(value = "login")
    public String login() {
        return "/page/login";
    }


}
