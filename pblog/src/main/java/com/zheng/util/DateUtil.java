package com.zheng.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zheng on 16/4/27.
 */
public class DateUtil {
    public static Date getDateByTimeStamp(String dateStr){
        Long timeStamp = Long.parseLong(dateStr)*1000;
        Date date = new Date(timeStamp);

        return date;
    }

    public static String getStrByTimeStamp(String timeStamp){
        Date date = getDateByTimeStamp(timeStamp);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(date);
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.getStrByTimeStamp("1460948677"));
    }

}
