package com.zheng.common;

import java.io.*;

/**
 * Created by zheng on 16/4/24.
 */
public class FileUtil {
    /**
     * 保存文件
     * @param data
     * @param filePath
     */
    public static void saveToLocal(byte[] data, String filePath) {
        try {
            DataOutputStream out = new DataOutputStream(new
                    FileOutputStream(new File(filePath)));
            for (int i = 0; i < data.length; i++) {
                out.write(data[i]);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void saveStr(String data, String filePath) {
        try {
            DataOutputStream out = new DataOutputStream(new
                    FileOutputStream(new File(filePath)));
            out.write(data.getBytes());
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /**
     * 读取文件
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getBytesFromFile(String url) throws IOException {
        File file = new File(url);
        InputStream is = new FileInputStream(file);
        // 获取文件大小
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // 文件太大，无法读取
            throw new IOException("File is to large " + file.getName());
        }
        // 创建一个数据来保存文件数据
        byte[] bytes = new byte[(int) length];
        // 读取数据到byte数组中
        int offset = 0;
        int numRead = 0;

        while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }
        // 确保所有数据均被读取
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    public static String getStreamString(InputStream inputStream) {
        if (inputStream != null) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer stringBuffer = new StringBuffer();
                String str = new String("");
                while ((str = bufferedReader.readLine()) != null) {
                    stringBuffer.append(str);
                }
                return stringBuffer.toString();
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        String str = new String(FileUtil.getBytesFromFile("/Users/zheng/downFile/carList0.txt"));
        System.out.println(str);
    }
}
