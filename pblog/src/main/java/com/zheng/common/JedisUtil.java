package com.zheng.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;

/**
 * Created by zheng on 16/4/25.
 */
public interface JedisUtil{

    public <T>String set(String key,T obj);

    public <T>T get(String key);

    public Long del(String key);

    public <T>void addList(String key,List<T> list);

    public <T>List<T> getList(String key);

    public Long listSize(String key);


}
