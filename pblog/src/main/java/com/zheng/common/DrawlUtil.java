package com.zheng.common;

import com.zheng.common.model.DrawlParameter;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;
import java.util.List;

/**
 * Created by zheng on 16/4/26.
 */
public class DrawlUtil {
    /**
     * post请求传一个参数
     * @param url
     * @param paramName
     * @param paramValue
     * @return
     */
    public static PostMethod urlPostMethod(String url, String paramName, String paramValue) {
        HttpClient httpClient = new HttpClient();
        PostMethod method = new PostMethod(url);
        try {
            method.setParameter(paramName, paramValue);
            method.releaseConnection();
            httpClient.executeMethod(method);

            return method;
        } catch (HttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * pots请求传多个参数
     * @param url
     * @param drawlParameterList
     * @return
     */
    public static PostMethod postMethodByParamList(String url, List<DrawlParameter> drawlParameterList) {
        HttpClient httpClient = new HttpClient();
        PostMethod method = new PostMethod(url);
        try {
            NameValuePair[] nameValuePairs = new NameValuePair[drawlParameterList.size()];
            for(int i=0;i<drawlParameterList.size();i++){
                NameValuePair nameValuePair = new NameValuePair();
                nameValuePair.setName(drawlParameterList.get(i).getParamName());
                nameValuePair.setValue(drawlParameterList.get(i).getParamValue());

                nameValuePairs[i] = nameValuePair;
            }
            method.addParameters(nameValuePairs);
            method.releaseConnection();
            httpClient.executeMethod(method);

            return method;
        } catch (HttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将unicode码转换为中文
     * @param utfString
     * @return
     */
    public static String convert(String utfString){
        StringBuilder sb = new StringBuilder();
        int i = -1;
        int pos = 0;

        while((i=utfString.indexOf("\\u", pos)) != -1){
            sb.append(utfString.substring(pos, i));
            if(i+5 < utfString.length()){
                pos = i+6;
                sb.append((char)Integer.parseInt(utfString.substring(i+2, i+6), 16));
            }
        }

        return sb.toString();
    }
}
