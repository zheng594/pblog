package com.zheng.common.impl;

import com.zheng.common.JedisUtil;
import com.zheng.common.SerializeUtil;
import com.zheng.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by zheng on 16/5/5.
 */
@Service
public class JedisUtilImpl implements JedisUtil{
    private static Logger logger = LoggerFactory.getLogger(JedisUtilImpl.class);

    @Autowired
    private JedisPool jedisPool;

    private Jedis getJedis(){
        if(jedisPool == null){
            logger.error("jedis连接异常");
            return null;
        }
        return jedisPool.getResource();
    }

    /**
     * 向redis添加对象
     * @param key
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> String set(String key, T clazz) {
        Jedis jedis = getJedis();
        try {
            return jedis.set(key.getBytes(), SerializeUtil.serialize(clazz));
        }catch (Exception e){
            logger.error("redis set Exception:"+e);
            return null;
        }
    }

    /**
     * 根据key获得对象
     * @param key
     * @param <T>
     * @return
     */
    public <T>T get(String key){
        Jedis jedis = getJedis();
        try {
            return SerializeUtil.deserialize(jedis.get(key.getBytes()));
        }catch (Exception e){
            logger.error("redis get Exception:"+e);
            return null;
        }
    }

    public Long del(String key) {
        Jedis jedis = getJedis();
        return jedis.del(key);
    }

    public <T> void addList(String key,List<T> list) {
        Jedis jedis = getJedis();
        for(T clazz:list){
            jedis.lpush(key.getBytes(),SerializeUtil.serialize(clazz));
        }
    }

    public <T>List<T> getList(String key){
        Jedis jedis = getJedis();

        List<byte[]> byteList = jedis.lrange(key.getBytes(),0l,jedis.llen(key)-1);
        List<T> list = new ArrayList<T>();
        for(byte[] bytes:byteList){
            list.add((T) SerializeUtil.deserialize(bytes));
        }
        return list;
    }

    public Long listSize(String key){
        Jedis jedis = getJedis();
        return jedis.llen(key);
    }


    public static void main(String[] args) {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        applicationContext.start();
        JedisUtil jedisUtil = (JedisUtilImpl) applicationContext.getBean("jedisUtilImpl");

        List<User> list = new ArrayList<User>();
        User user2 = new User();
        user2.setUserName("zheng");
        user2.setPassword("1111");
        list.add(user2);

        User user1 = new User();
        user1.setUserName("wang");
        user1.setPassword("2222");
        list.add(user1);

//        jedisUtil.addList("userList",list);
        List<User> userList = jedisUtil.getList("userList");
        for(User u:userList){
            logger.warn(String.valueOf(u));
        }

//        logger.warn("length:"+jedisUtil.listSize("userList"));

//        List<String> testList = new ArrayList<String>();
//        testList.add("112222");
//        testList.add("333333");
//
//        jedisUtil.addList("testList",testList);
//        List<String> l1 = jedisUtil.getList("testList");
//        for(String l:l1){
//            logger.warn(l);
//        }




    }
}
