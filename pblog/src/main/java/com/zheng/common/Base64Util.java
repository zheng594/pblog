package com.zheng.common;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by zheng on 16/4/24.
 */
public class Base64Util {
    public static String encoder(String str){
        byte[] arr = Base64.encodeBase64(str.getBytes());
        return new String(arr);
    }

    public static void main(String[] args) {
        System.out.println(Base64Util.encoder("S1512210323"));
    }
}
