package com.zheng.common;

import com.zheng.model.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Created by zheng on 16/5/6.
 */
public class SerializeUtil {
    private static Logger logger = LoggerFactory.getLogger(SerializeUtil.class);

    /**
     * 序列化对象
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> byte[] serialize(T clazz){
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(clazz);

            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            logger.error("序列化对象出错:"+e);
            return null;
        }
    }

    /**
     * 反序列化
     * @param bytes
     * @param <T>
     * @return
     */
    public static <T>T deserialize(byte[] bytes){
        ObjectInputStream objectInputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(bytes);
            objectInputStream = new ObjectInputStream(byteArrayInputStream);

            return (T) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            logger.error("反序列化对象出错:"+e);
            return null;
        } catch (IOException e) {
            logger.error("反序列化对象出错:"+e);
            return null;
        }
    }

//    public static void main(String[] args) {
//        User user = new User();
//        user.setUserName("zheng");
//        user.setPassword("1111");
//
//        byte[] bytes = SerializeUtil.serialize(user);
//        User user1 =  SerializeUtil.deserialize(bytes);
//        logger.warn(String.valueOf(user1));
//    }
}
