package com.zheng.common.model;

import lombok.Data;

/**
 * Created by zheng on 16/5/6.
 */
@Data
public class DrawlParameter {
    private String paramName;
    private String paramValue;
}
