package com.zheng.service;

import com.zheng.dao.user.UserDao;
import com.zheng.model.user.User;
import com.zheng.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by user on 2016/3/31.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

//    public User getUser(User user) {
//        return userDao.getUser(user);
//    }

    public int getUserCount() {
        return userDao.getUserCount();
    }

}
