import com.zheng.common.JedisUtil;
import com.zheng.model.user.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by zheng on 16/4/26.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@Slf4j
public class JedisTest {
    @Autowired
    private JedisUtil jedisUtil;

    @Test
    public void testJedis(){
        User user = new User();
        user.setUserName("zheng");
        user.setPassword("1111");
        jedisUtil.set("user",user);

        User u = jedisUtil.get("user");
        log.warn(String.valueOf(u));

    }



}
