/**
 * Created by zheng on 16/5/8.
 */
$(document).ready(function() {
    var headerStr = "<div class='header' id='header'></div>"+
            "<nav class='navbar navbar-inverse navbar-fixed-top' role='navigation'>"+
            "    <div class='container'>"+
            "        <div class='navbar-header'>"+
            "            <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1'>"+
            "                <span class='sr-only'>Toggle navigation</span>"+
            "                <span class='icon-bar'></span>"+
            "                <span class='icon-bar'></span>"+
            "                <span class='icon-bar'></span>"+
            "            </button>"+
            "            <a class='navbar-brand' href='index.html'>主页</a>"+
            "        </div>"+
            "        <div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>"+
            "            <ul class='nav navbar-nav navbar-right'>"+
            "                <li>"+
            "                    <a href='/pblog/page/login.html'>登录</a>"+
            "                </li>"+
            "                <li>"+
            "                    <a href='#'>我</a>"+
            "                </li>"+
            "                <li>"+
            "                    <a href='blogList.html'>博客</a>"+
            "                </li>"+
            "                <li>"+
            "                    <a href='contact.html'>联系我</a>"+
            "                </li>"+
            "                <li>"+
            "                    <a href='#'>FAQ</a>"+
            "                </li>"+
            "                <li>"+
            "                    <a href='#'>&nbsp;</a>"+
            "                </li>"+
            "            </ul>"+
            "        </div>"+
            "    </div>"+
            "</nav>";
    $("#header").append(headerStr)
})